let oneQuantity = 0;
let twoQuantity = 0;
let fiveQuantity = 0;
let tenQuantity = 0;
let twentyQuantity = 0;
let fiftyQuantity = 0;
let onehundredQuantity = 0;

//set up Event Listener
function initialize() {
    document.getElementById('one-quantity').addEventListener('keyup', onOneChange);
    document.getElementById('two-quantity').addEventListener('keyup', (e) => {
        twoQuantity = e.target.value;
        calculator();
    })
    document.getElementById('five-quantity').addEventListener('keyup', onFiveChange);
    document.getElementById('ten-quantity').addEventListener('keyup', onTenChange);
    document.getElementById('twenty-quantity').addEventListener('keyup', onTwentyChange);
    document.getElementById('fifty-quantity').addEventListener('keyup', (e) => {
        fiftyQuantity = e.target.value;
        calculator();
    });
    document.getElementById('one-hundred-quantity').addEventListener('keyup', (e) => {
        onehundredQuantity = e.target.value;
        calculator();
    });
    
    calculator();
};

//function get call

function onOneChange (e){
    oneQuantity = e.target.value;
    calculator();
};

function onFiveChange(e){
    fiveQuantity = e.target.value;
    calculator();
};

function onTenChange(e){
    tenQuantity = e.target.value;
    calculator();
};

function onTwentyChange(e){
    twentyQuantity = e.target.value;
    calculator();
};

//calculator
function calculator (){
    //calculator amount
    var oneAmount = 1 * oneQuantity;
    var twoAmount = 2 * twoQuantity;
    var fiveAmount = 5 * fiveQuantity;
    var tenAmount = 10 * tenQuantity;
    var twentyAmount = 20 * twentyQuantity;
    var fiftyAmount = 50 * fiftyQuantity;
    var onehundredAmount = 100 * onehundredQuantity;

    //total amount
    var total = oneAmount + twoAmount + fiveAmount + tenAmount + twentyAmount + fiftyAmount + onehundredAmount;

    //display the amount
    document.getElementById('one-amount').innerHTML = formatCurrency(oneAmount);
    document.getElementById('two-amount').innerHTML = formatCurrency(twoAmount);
    document.getElementById('five-amount').innerHTML = formatCurrency(fiveAmount);
    document.getElementById('ten-amount').innerHTML = formatCurrency(tenAmount);
    document.getElementById('twenty-amount').innerHTML = formatCurrency(twentyAmount);
    document.getElementById('fifty-amount').innerHTML = formatCurrency(fiftyAmount);
    document.getElementById('one-hundred-amount').innerHTML = formatCurrency(onehundredAmount);
    document.getElementById('total').innerHTML = formatCurrency(total);

};

//format currency
   function formatCurrency (amount){
    var formattedAmount = amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    return '$' + formattedAmount;
   }

initialize();